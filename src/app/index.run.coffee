angular.module 'rentaloTodoFrontend'
  .run ($rootScope, $auth, $state, ngNotify) ->
    'ngInject'
    ngNotify.config
      theme: 'pure',
      position: 'top',
      duration: 3000,
      type: 'success',
      sticky: false,
      button: true,
      html: false
    $rootScope.$on 'auth:login-success', (ev, user) ->
      $state.go "home.me.dashboard"
    $rootScope.$on 'auth:registration-email-success', (ev, user) ->
      $state.go "session.login"
    $rootScope.$on 'auth:logout-success', (ev, user) ->
      $state.go "home.landing"
    $rootScope.$on 'auth:login-error', (ev, reason) ->
      console.log reason
      $state.go "session.login"
    $rootScope.$on 'auth:registration-email-error', (ev, reason) ->
      console.log reason
      $state.go "session.register"
    $rootScope.$on 'auth:email-confirmation-success', (ev, user) ->
      $state.go "home.me.dashboard"
    $rootScope.$on 'auth:password-reset-request-success', (ev, data) ->
      $state.go "session.login"
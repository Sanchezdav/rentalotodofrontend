angular.module 'rentaloTodoFrontend'
  .config ($stateProvider, $urlRouterProvider, $locationProvider) ->
    'ngInject'
    $stateProvider
      .state 'session',
        templateUrl: "app/views/templates/session.html"
        abstract: true
    $stateProvider
      .state 'session.login',
        url: "/ingresar"
        templateUrl: "app/views/session/login.html"
        controller:"LoginController"
    $stateProvider
      .state 'session.register',
        url: "/registrarse"
        templateUrl: "app/views/session/register.html"
        controller:"RegisterController"
    $stateProvider
      .state 'session.confirmation',
        url: "/confirmacion"
        templateUrl: "app/views/session/confirmation.html"
    $stateProvider
      .state 'session.user_email',
        url: "/email"
        templateUrl: "app/views/session/email.html"
        controller:"EmailController"
    $stateProvider
      .state 'session.password_recovery',
        url: "/recuperar_password"
        templateUrl: "app/views/session/password_recovery.html"
        controller:"PasswordRecoveryController"
    $stateProvider
      .state 'home',
        abstract: true
        templateUrl: "app/views/templates/home.html"
        controller: "HomeController"
    $stateProvider
      .state 'home.landing',
        url: "/"
        templateUrl: "app/views/landing.html"
        controller:"LandingController"
    $stateProvider
      .state 'home.me',
        abstract:true
        templateUrl: "app/views/templates/me.html"
        resolve:
          auth: ($auth, $state) ->
            auth = $auth.validateUser()
            auth.then ->
              false
            .catch (data)->
              $state.go 'home.landing'
              false
            return auth
    $stateProvider
      .state 'home.me.dashboard',
        url: "/dashboard"
        templateUrl: "app/views/me/dashboard.html"
        controller:"DashboardController"
    $stateProvider
      .state 'home.me.profile',
        url: "/perfil"
        templateUrl: "app/views/me/profile.html"
        controller:"ProfileController"
    $stateProvider
      .state 'home.me.publications',
        url: "/mis_publicaciones"
        templateUrl: "app/views/me/publications.html"
        controller:"MePublicationsController"
    $stateProvider
      .state 'home.me.publication',
        url: "/mi_publicacion/:slug"
        templateUrl: "app/views/me/publication/publication.html"
        controller:"MePublicationController"
    $stateProvider
      .state 'home.me.publication.general',
        url: "/general"
        templateUrl: "app/views/me/publication/general.html"
        controller:"MePublicationGeneralController"
    $stateProvider
      .state 'home.me.publication.location',
        url: "/ubicacion"
        templateUrl: "app/views/me/publication/location.html"
        controller:"MePublicationLocationController"
    $stateProvider
      .state 'home.me.publication.image',
        url: "/portada"
        templateUrl: "app/views/me/publication/image.html"
        controller:"MePublicationImageController"
    $stateProvider
      .state 'home.me.publication.images',
        url: "/imagenes"
        templateUrl: "app/views/me/publication/images.html"
        controller:"MePublicationImagesController"
    $stateProvider
      .state 'home.me.publication.video',
        url: "/video"
        templateUrl: "app/views/me/publication/video.html"
        controller:"MePublicationVideoController"
    $stateProvider
      .state 'home.me.publication.characteristics',
        url: "/caracteristicas"
        templateUrl: "app/views/me/publication/characteristics.html"
        controller:"MePublicationCharacteristicsController"
    $stateProvider
      .state 'home.me.publication.options',
        url: "/opciones"
        templateUrl: "app/views/me/publication/options.html"
        controller:"MePublicationOptionsController"
    $stateProvider
      .state 'home.me.publication.additionals',
        url: "/adicionales"
        templateUrl: "app/views/me/publication/additionals.html"
        controller:"MePublicationAdditionalsController"

    $urlRouterProvider.otherwise '/'
    $locationProvider.html5Mode(true)

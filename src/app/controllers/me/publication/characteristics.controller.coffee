angular.module 'rentaloTodoFrontend'
  .controller 'MePublicationCharacteristicsController', ($scope, $state, Restangular, ngNotify, lodash, $rootScope, $mdDialog) ->
    'ngInject'
    $rootScope.section =
      title: "Administra tu publicación",
      subtitle:"Características"
    $scope.getCharacteristics = ->
      # Restangular.all("text_details").getList({subcategory: $scope.publication.subcategory.slug}).then (response) ->
      #   $scope.characteristics = response
      $scope.publication.all('publication_text_details').getList().then (response) ->
        $scope.my_details = response
        if $scope.my_details.length > 0
          $scope.text_details = {}
    $scope.newCharacteristic = (ev) ->
      $mdDialog.show
        targetEvent: ev,
        controller: 'NewCharacteristicController',
        templateUrl: 'app/views/dialogs/new_characteristic_dialog.html',
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true
    $scope.saveMyDetails = (details) ->
      $scope.loading = true
      lodash.forEach details, (detail, key) ->
        detail.answer = detail.answer
        detail.put({answer: detail.answer}).then (response) ->
          $scope.loading = false
          ngNotify.set('¡Se guardó correctamente!', 'success')
          if !$scope.publication.publishable
            $state.go $state.current , {}, { reload: true }
    if !$scope.publication?
      $rootScope.$on 'publicationGetted', ->
        $scope.getCharacteristics()
    else
      $scope.getCharacteristics()
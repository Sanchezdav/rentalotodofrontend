angular.module 'rentaloTodoFrontend'
  .controller 'NewLocationController', ($scope, $rootScope, $state, Restangular, ngNotify, $mdDialog) ->
    'ngInject'
    $scope.publication.location = {}
    $scope.states = [
      {name: 'Aguascalientes', slug: 'aguascalientes'},
      {name: 'Baja California', slug: 'baja-california'},
      {name: 'Baja California Sur', slug: 'baja-california-sur'},
      {name: 'Campeche', slug: 'campeche'},
      {name: 'Chiapas', slug: 'chiapas'},
      {name: 'Chihuahua', slug: 'chihuahua'},
      {name: 'Coahuila', slug: 'coahuila'},
      {name: 'Colima', slug: 'colima'},
      {name: 'Distrito Federal', slug: 'distrito-federal'},
      {name: 'Durango', slug: 'durango'},
      {name: 'Estado de Mexico', slug: 'estado-de-mexico'},
      {name: 'Guanajuato', slug: 'guanajuato'},
      {name: 'Guerrero', slug: 'guerrero'},
      {name: 'Hidalgo', slug: 'hidalgo'},
      {name: 'Jalisco', slug: 'jalisco'},
      {name: 'Michoacan', slug: 'michoacan'},
      {name: 'Morelos', slug: 'morelos'},
      {name: 'Nayarit', slug: 'nayarit'},
      {name: 'Nuevo Leon', slug: 'nuevo-leon'},
      {name: 'Oaxaca', slug: 'oaxaca'},
      {name: 'Puebla', slug: 'puebla'},
      {name: 'Queretaro', slug: 'queretaro'},
      {name: 'Quintana Roo', slug: 'quintana-roo'},
      {name: 'San Luis Potosi', slug: 'san-luis-potosi'},
      {name: 'Sinaloa', slug: 'sinaloa'},
      {name: 'Sonora', slug: 'sonora'},
      {name: 'Tabasco', slug: 'tabasco'},
      {name: 'Tamaulipas', slug: 'tamaulipas'},
      {name: 'Tlaxcala', slug: 'tlaxcala'},
      {name: 'Veracruz', slug: 'veracruz'},
      {name: 'Yucatan', slug: 'yucatan'},
      {name: 'Zacatecas', slug: 'zacatecas'}
    ]
    $scope.saveLocation = ->
      $scope.loading = true
      $scope.publication.all("locations").post($scope.publication.location).then (response) ->
        $scope.loading = false
        $mdDialog.hide()
        ngNotify.set('¡Se guardó correctamente!', 'success')
      .catch (response) ->
        $scope.loading = false
        ngNotify.set('¡No se pudo guardar, intente de nuevo!', 'error')
    $scope.cancel = ->
      $mdDialog.cancel()
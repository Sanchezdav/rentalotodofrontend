angular.module 'rentaloTodoFrontend'
  .controller 'MePublicationImagesController', ($scope, $state, Restangular, apiUrl, $rootScope, Upload, ngNotify, lodash) ->
    'ngInject'
    $rootScope.section =
      title: "Administra tu publicación",
      subtitle:"Galería de imágenes"
    $scope.url = apiUrl
    $scope.upload = (files)->
      $scope.loading = true
      Upload.upload
        url: $scope.publication.getRequestedUrl() + "/publication_images"
        file: files
        method: 'POST'
      .success (data, status, headers, config)->
        $scope.loading = false
        ngNotify.set('¡Se guardó correctamente!', 'success')
        $scope.images.unshift(data)
        $scope.myfiles = null
        files = null
      .error (data, status, headers, config) ->
        $scope.loading = false
        ngNotify.set('¡Ocurrió un error!', 'error')
    $scope.uploadCancel = ->
      $scope.myfiles = null
      $scope.files = null
    $scope.uploadFiles = (files) ->
      $scope.myfiles = files
    $scope.deleteImage = (image) ->
      image.remove().then ->
        lodash.remove $scope.images, (item) ->
          item.id == image.id
    getImages = ->
      $scope.publication.all("publication_images").getList().then (response) ->
        $scope.images = response
    if !$scope.publication?
      $rootScope.$on 'publicationGetted', ->
        getImages()
    else
      getImages()
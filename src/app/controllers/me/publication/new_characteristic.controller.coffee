angular.module 'rentaloTodoFrontend'
  .controller 'NewCharacteristicController', ($scope, $rootScope, $state, Restangular, ngNotify, $mdDialog, lodash) ->
    'ngInject'
    $scope.list = []
    $scope.list2 = []
    $scope.cancel = ->
      $mdDialog.cancel()
    getMyTextDetails = ->
      Restangular.all('text_details').getList({subcategory: $scope.publication.subcategory.slug}).then (response) ->
        $scope.text_details = response
        $scope.publication.all('publication_text_details').getList().then (response) ->
          $scope.pub_details = response
          if $scope.pub_details.length > 0
            lodash.forEach $scope.text_details, (item, key) ->
              lodash.forEach $scope.pub_details, (item2, key) ->
                if item.id == item2.text_detail_id
                  $scope.list.push(item)
    $scope.detailSelected = (detail) ->
      idx = $scope.list.indexOf(detail)
      if idx > -1
        $scope.list.splice idx, 1
      else
        $scope.list.push detail
    $scope.myDetailSelected = (detail) ->
      idx = $scope.list2.indexOf(detail)
      if idx > -1
        $scope.list2.splice idx, 1
      else
        $scope.list2.push detail
    $scope.addDetails = ->
      $scope.loadingNew = true
      $scope.publication.all("publication_text_details").post({list: $scope.list}).then (response) ->
        $scope.loadingNew = false
        $mdDialog.hide()
        ngNotify.set('¡Se agregaron correctamente!', 'success')
        $scope.getCharacteristics()
        $scope.getValidations()
      .catch (response) ->
        $scope.loadingNew = false
        ngNotify.set('¡No se pudo agregar, intente de nuevo!', 'error')
    $scope.removeDetails = ->
      $scope.loading2 = true
      lodash.forEach $scope.list2, (detail, key) ->
        detail.remove().then (response) ->
          $scope.loading2 = false
          $mdDialog.hide()
          ngNotify.set('¡Se eliminó correctamente!', 'success')
          $scope.getCharacteristics()
    if !$scope.publication?
      $rootScope.$on 'publicationGetted', ->
        getMyTextDetails()
    else
      getMyTextDetails()
angular.module 'rentaloTodoFrontend'
  .controller 'NewOptionsController', ($scope, $rootScope, $state, Restangular, ngNotify, $mdDialog, lodash) ->
    'ngInject'
    $scope.list = []
    $scope.cancel = ->
      $mdDialog.cancel()
    getMyOptionDetails = ->
      Restangular.all('option_details').getList({subcategory: $scope.publication.subcategory.slug}).then (response) ->
        $scope.option_details = response
        $scope.publication.all('publication_option_details').getList().then (response) ->
          $scope.pub_details = response
          if $scope.pub_details.length > 0
            lodash.forEach $scope.option_details, (item, key) ->
              lodash.forEach $scope.pub_details, (item2, key) ->
                if item.id == item2.option_detail_id
                  $scope.list.push(item)
    $scope.detailSelected = (detail) ->
      idx = $scope.list.indexOf(detail)
      if idx > -1
        $scope.list.splice idx, 1
      else
        $scope.list.push detail
    $scope.addDetails = ->
      $scope.loadingNew = true
      $scope.publication.all("publication_option_details").post({list: $scope.list}).then (response) ->
        $scope.loadingNew = false
        $mdDialog.hide()
        ngNotify.set('¡Se agregaron correctamente!', 'success')
        $scope.getOptions()
        $scope.getValidations()
      .catch (response) ->
        $scope.loadingNew = false
        ngNotify.set('¡No se pudo agregar, intente de nuevo!', 'error')
    if !$scope.publication?
      $rootScope.$on 'publicationGetted', ->
        getMyOptionDetails()
    else
      getMyOptionDetails()
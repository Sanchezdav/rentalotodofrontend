angular.module 'rentaloTodoFrontend'
  .controller 'NewAdditionalsController', ($scope, $rootScope, $state, Restangular, Pagination, ngNotify, $mdDialog, lodash) ->
    'ngInject'
    $scope.list = []
    $scope.cancel = ->
      $mdDialog.cancel()
    getMyAdditionalDetails = ->
      Restangular.all('additional_details').getList({subcategory: $scope.publication.subcategory.slug}).then (response) ->
        $scope.additional_details = response
        if $scope.additional_details
          $scope.additional_details.pagination = Pagination.getNew(9)
          $scope.additional_details.pagination.numPages = Math.ceil($scope.additional_details.length/$scope.additional_details.pagination.perPage)
        $scope.publication.all('publication_plus_details').getList().then (response) ->
          $scope.pub_details = response
          if $scope.pub_details.length > 0
            lodash.forEach $scope.additional_details, (item, key) ->
              lodash.forEach $scope.pub_details, (item2, key) ->
                if item.id == item2.additional_detail_id
                  $scope.list.push(item)
    $scope.detailSelected = (detail) ->
      idx = $scope.list.indexOf(detail)
      if idx > -1
        $scope.list.splice idx, 1
      else
        $scope.list.push detail
    $scope.addDetails = ->
      $scope.loadingNew = true
      $scope.publication.all("publication_plus_details").post({list: $scope.list}).then (response) ->
        $scope.loadingNew = false
        $mdDialog.hide()
        ngNotify.set('¡Se agregaron correctamente!', 'success')
        $scope.getAdditionals()
        # $scope.getValidations()
      .catch (response) ->
        $scope.loadingNew = false
        ngNotify.set('¡No se pudo agregar, intente de nuevo!', 'error')
    if !$scope.publication?
      $rootScope.$on 'publicationGetted', ->
        getMyAdditionalDetails()
    else
      getMyAdditionalDetails()
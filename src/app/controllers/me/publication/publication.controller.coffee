angular.module 'rentaloTodoFrontend'
  .controller 'MePublicationController', ($scope, $state, $rootScope, Restangular, $stateParams, ngNotify, $mdDialog) ->
    'ngInject'
    $rootScope.section =
      title: "Administra tu publicación"
    $scope.getValidations = ->
      $scope.publication.all('publication_validations').getList().then (response) ->
        $scope.publication.validations = response
    Restangular.all('me').one('publications', $stateParams.slug).get().then (response) ->
      $scope.publication = response
      $scope.getValidations()
      $rootScope.$broadcast('publicationGetted')
    $scope.updatePublication = ->
      $scope.loading = true
      $scope.publication.put().then (response) ->
        $scope.loading = false
        ngNotify.set('¡Se guardó correctamente!', 'success')
        $scope.getValidations()
      .catch (response) ->
        $scope.loading = false
        ngNotify.set('¡No se pudo guardar, intente de nuevo!', 'error')
    $scope.deletePublicationConfirm = (ev)->
      confirm = $mdDialog.confirm()
        .title('¿Estás seguro que deseas eliminar la publicación?')
        .content('Perderás toda la información')
        .ariaLabel('¿Estás seguro que deseas eliminar la publicación?')
        .targetEvent(ev)
        .ok('Aceptar')
        .cancel('cancelar')
      $mdDialog.show(confirm).then(->
        $scope.deletePublication()
      , ->
        )
      false
    $scope.deletePublication = ->
      $scope.publication.remove().then ->
        $state.go 'home.me.publications'
    $scope.publishPublication = ->
      if $scope.publication.published == false
        $scope.publication.published = true
        $scope.publication.put().then (response) ->
          ngNotify.set('¡Se publicó correctamente!', 'success')
        .catch (response) ->
          ngNotify.set('¡No se pudo publicar, intente de nuevo!', 'error')
      else
        $scope.publication.published = false
        $scope.publication.put().then (response) ->
          ngNotify.set('¡Se eliminó del público correctamente!', 'success')
        .catch (response) ->
          ngNotify.set('¡No se pudo eliminar del público, intente de nuevo!', 'error')

angular.module 'rentaloTodoFrontend'
  .controller 'MePublicationLocationController', ($scope, $state, Restangular, $rootScope, $mdDialog, ngNotify) ->
    'ngInject'
    $rootScope.section =
      title: "Administra tu publicación",
      subtitle:"Ubicación"
    $scope.states = [
      {name: 'Aguascalientes', slug: 'aguascalientes'},
      {name: 'Baja California', slug: 'baja-california'},
      {name: 'Baja California Sur', slug: 'baja-california-sur'},
      {name: 'Campeche', slug: 'campeche'},
      {name: 'Chiapas', slug: 'chiapas'},
      {name: 'Chihuahua', slug: 'chihuahua'},
      {name: 'Coahuila', slug: 'coahuila'},
      {name: 'Colima', slug: 'colima'},
      {name: 'Distrito Federal', slug: 'distrito-federal'},
      {name: 'Durango', slug: 'durango'},
      {name: 'Estado de Mexico', slug: 'estado-de-mexico'},
      {name: 'Guanajuato', slug: 'guanajuato'},
      {name: 'Guerrero', slug: 'guerrero'},
      {name: 'Hidalgo', slug: 'hidalgo'},
      {name: 'Jalisco', slug: 'jalisco'},
      {name: 'Michoacan', slug: 'michoacan'},
      {name: 'Morelos', slug: 'morelos'},
      {name: 'Nayarit', slug: 'nayarit'},
      {name: 'Nuevo Leon', slug: 'nuevo-leon'},
      {name: 'Oaxaca', slug: 'oaxaca'},
      {name: 'Puebla', slug: 'puebla'},
      {name: 'Queretaro', slug: 'queretaro'},
      {name: 'Quintana Roo', slug: 'quintana-roo'},
      {name: 'San Luis Potosi', slug: 'san-luis-potosi'},
      {name: 'Sinaloa', slug: 'sinaloa'},
      {name: 'Sonora', slug: 'sonora'},
      {name: 'Tabasco', slug: 'tabasco'},
      {name: 'Tamaulipas', slug: 'tamaulipas'},
      {name: 'Tlaxcala', slug: 'tlaxcala'},
      {name: 'Veracruz', slug: 'veracruz'},
      {name: 'Yucatan', slug: 'yucatan'},
      {name: 'Zacatecas', slug: 'zacatecas'}
    ]
    $scope.newLocation = (ev) ->
      $mdDialog.show
        targetEvent: ev,
        controller: 'NewLocationController',
        templateUrl: 'app/views/dialogs/new_location_dialog.html',
        clickOutsideToClose: true,
        scope: $scope.$new()
    $scope.updateLocation = ->
      $scope.publication.one("locations", $scope.publication.location.id).get().then (response) ->
        my_location = response
        my_location.country = $scope.publication.location.country
        my_location.state = $scope.publication.location.state
        my_location.address = $scope.publication.location.address
        my_location.address_line_2 = $scope.publication.location.address_line_2
        my_location.city = $scope.publication.location.city
        my_location.zip_code = $scope.publication.location.zip_code
        my_location.put().then (response) ->
          $scope.loading = false
          ngNotify.set('¡Se actualizó correctamente!', 'success')
        .catch (response) ->
          $scope.loading = false
          ngNotify.set('¡No se pudo actualizar, intente de nuevo!', 'error')
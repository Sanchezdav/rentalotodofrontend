angular.module 'rentaloTodoFrontend'
  .controller 'MePublicationImageController', ($scope, $state, Restangular, apiUrl, $rootScope, Upload, ngNotify) ->
    'ngInject'
    $rootScope.section =
      title: "Administra tu publicación",
      subtitle:"Imagen de portada"
    $scope.url = apiUrl
    $scope.upload = (file)->
      $scope.loading = true
      Upload.upload
        url: $scope.publication.getRequestedUrl()
        file: file
        method: 'PUT'
      .success (data, status, headers, config)->
        if !$scope.publication?
          $rootScope.$on 'publicationGetted', ->
            $scope.publication.image.image.preview.url = data.image.image.preview.url
            $scope.publication.image.image.url = data.image.image.url
        else
          $scope.publication.image.image.preview.url = data.image.image.preview.url
          $scope.publication.image.image.url = data.image.image.url
        $scope.loading = false
        ngNotify.set('¡Se guardó correctamente!', 'success')
        $scope.my_image = null
      .error (data, status, headers, config) ->
        $scope.loading = false
        ngNotify.set('¡Ocurrió un error!', 'error')
    $scope.uploadCancel = ->
      $scope.my_image = null
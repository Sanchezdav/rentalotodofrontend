angular.module 'rentaloTodoFrontend'
  .controller 'MePublicationGeneralController', ($scope, $state, Restangular, $rootScope, $timeout) ->
    'ngInject'
    $rootScope.section =
      title: "Administra tu publicación",
      subtitle:"Información general"
    Restangular.all('categories').getList().then (response)->
      $scope.categories = response
    Restangular.all('publication_types').getList().then (response)->
      $scope.types = response
    $scope.subcategories = []
    $scope.getSubcategories = ->
      if !$scope.publication.category.slug?
        $scope.subcategories = []
        return
      Restangular.all('subcategories').getList({category: $scope.publication.category.slug}).then (response)->
        $scope.subcategories = response
    if !$scope.publication?
      $rootScope.$on 'publicationGetted', ->
        $scope.getSubcategories()
        $scope.publication.subcategory_id = $scope.publication.subcategory.id
    else
      $scope.getSubcategories()
      $scope.publication.subcategory_id = $scope.publication.subcategory.id
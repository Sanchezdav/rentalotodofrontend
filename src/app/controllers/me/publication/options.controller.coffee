angular.module 'rentaloTodoFrontend'
  .controller 'MePublicationOptionsController', ($scope, $state, Restangular, ngNotify, lodash, $rootScope, $mdDialog) ->
    'ngInject'
    $rootScope.section =
      title: "Administra tu publicación",
      subtitle:"Opciones"
    $scope.getOptions = ->
      $scope.publication.all('publication_option_details').getList().then (response) ->
        $scope.my_details = response
        if $scope.my_details.length > 0
          $scope.option_details = {}
    $scope.newOptions = (ev) ->
      $mdDialog.show
        targetEvent: ev,
        controller: 'NewOptionsController',
        templateUrl: 'app/views/dialogs/new_options_dialog.html',
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true
    $scope.saveMyDetails = (details) ->
      $scope.loading = true
      lodash.forEach details, (detail, key) ->
        detail.answer = detail.answer
        detail.put({option_id: detail.option_id}).then (response) ->
          $scope.loading = false
          ngNotify.set('¡Se guardó correctamente!', 'success')
          if !$scope.publication.publishable
            $state.go $state.current , {}, { reload: true }
    if !$scope.publication?
      $rootScope.$on 'publicationGetted', ->
        $scope.getOptions()
    else
      $scope.getOptions()
angular.module 'rentaloTodoFrontend'
  .controller 'MePublicationAdditionalsController', ($scope, $state, Restangular, ngNotify, lodash, $rootScope, $mdDialog) ->
    'ngInject'
    $rootScope.section =
      title: "Administra tu publicación",
      subtitle:"Adicionales"
    $scope.getAdditionals = ->
      $scope.publication.all('publication_plus_details').getList().then (response) ->
        $scope.my_details = response
        if $scope.my_details.length > 0
          $scope.additional_details = {}
    $scope.newAdditionals = (ev) ->
      $mdDialog.show
        targetEvent: ev,
        controller: 'NewAdditionalsController',
        templateUrl: 'app/views/dialogs/new_additionals_dialog.html',
        clickOutsideToClose: true,
        scope: $scope,
        preserveScope: true
    $scope.saveMyDetails = (details) ->
      $scope.loading = true
      lodash.forEach details, (detail, key) ->
        detail.answer = detail.answer
        detail.put({answer: detail.answer}).then (response) ->
          $scope.loading = false
          ngNotify.set('¡Se guardó correctamente!', 'success')
          $scope.getAdditionals()
          # if !$scope.publication.publishable
          #   $state.go $state.current , {}, { reload: true }
    if !$scope.publication?
      $rootScope.$on 'publicationGetted', ->
        $scope.getAdditionals()
    else
      $scope.getAdditionals()
angular.module 'rentaloTodoFrontend'
  .controller 'NewPublicationController', ($scope, $rootScope, $state, Restangular, ngNotify, $mdDialog) ->
    'ngInject'
    $scope.publication = {}
    $scope.subcategories = []
    Restangular.all('categories').getList().then (response)->
      $scope.categories = response
    Restangular.all('publication_types').getList().then (response)->
      $scope.types = response
    $scope.getSubcategories = ->
      if !$scope.publication.category.slug?
        $scope.subcategories = []
        return
      Restangular.all('subcategories').getList({category: $scope.publication.category.slug}).then (response)->
        $scope.subcategories = response
    $scope.cancel = ->
      $mdDialog.cancel()
    $scope.savePublication = ->
      $scope.loading = true
      Restangular.all('me').all('publications').post($scope.publication).then (response) ->
        $scope.loading = false
        $scope.publications.unshift(response)
        $mdDialog.hide()
        $state.go 'home.me.publication.general', slug: response.slug
        ngNotify.set('¡Se guardó correctamente!', 'success')
      .catch (response) ->
        $scope.loading = false
        ngNotify.set('¡No se pudo guardar, intente de nuevo!', 'error')
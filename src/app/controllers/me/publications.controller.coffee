angular.module 'rentaloTodoFrontend'
  .controller 'MePublicationsController', ($scope, $rootScope, $mdDialog, Restangular) ->
    'ngInject'
    $rootScope.section =
      title:"Mis publicaciones",
      subtitle:"Ve todas tus publicaciones"
    Restangular.all("me").all("publications").getList().then (response) ->
      $scope.publications = response
    $scope.newPublication = (ev) ->
      $mdDialog.show
        targetEvent: ev,
        controller: 'NewPublicationController',
        templateUrl: 'app/views/dialogs/new_publication_dialog.html',
        clickOutsideToClose: true,
        scope: $scope.$new()
angular.module 'rentaloTodoFrontend'
  .controller 'LoginController', ($scope, $auth, $location, $state) ->
    'ngInject'
    $scope.login = {}
    if $location.search().auth_token
      $state.go "home.me.dashboard"

    $scope.loadingCircle = ->
      $scope.loading = true

    $scope.loginSubmit = ->
      $scope.loading = true
      $scope.loginLoading = true
      $auth.submitLogin $scope.login
      .then (resp)->
        $scope.loginLoading = false
      .catch (resp) ->
        $scope.loginLoading = false
        $scope.errors = resp.errors

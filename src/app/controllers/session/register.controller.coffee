angular.module 'rentaloTodoFrontend'
  .controller 'RegisterController', ($scope, $location, $state, ngNotify, $auth) ->
    'ngInject'
    $scope.register = {}
    if $location.search().auth_token
      $state.go "home.me.dashboard"

    $scope.loadingCircle = ->
      $scope.loading = true

    $scope.registrationSubmit= ->
      $scope.loading = true
      $scope.loginLoading = true
      $auth.submitRegistration $scope.register
      .then (resp)->
        $scope.loginLoading = false
        ngNotify.set('¡Se te envió un correo, por favor confirma tu email!', 'info')
        $state.go "session.confirmation"
      .catch (resp) ->
        $scope.loginLoading = false
        ngNotify.set('¡No se pudo enviar el correo, por favor intenta de nuevo!', 'error')
angular.module 'rentaloTodoFrontend'
  .controller 'EmailController', ($scope, $auth, $state, $timeout, ngNotify) ->
    'ngInject'
    $scope.updateEmail = ->
      $scope.updateLoading = true
      $auth.updateAccount($scope.updateAccountForm).then ->
        $scope.updateLoading = false
        ngNotify.set('¡Se guardó correctamente!', 'success')
        $timeout (->
          $state.go "home.me.dashboard"
          )
          ,1e3
      .catch (res)->
        $scope.errors = res.data.errors.full_messages
        $scope.updateLoading = false
        # ngNotify.set('¡No se pudo guardar, intente de nuevo!', 'error')
    false




angular.module 'rentaloTodoFrontend'
  .controller 'PasswordRecoveryController', ($scope, $auth) ->
    'ngInject'
    $scope.recoveryPassword = ->
      $scope.loading = true
      $auth.requestPasswordReset $scope.recovery
      .then ->
        $scope.loading = false
        ngNotify.set('¡Se ha enviado un correo electrónico a la dirección que ingresaste con las instrucciones para continuar!', 'success')
      .catch ->
        $scope.loading = false
        ngNotify.set('¡Ocurrió un error, intente de nuevo!', 'info')
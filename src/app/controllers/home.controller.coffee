angular.module 'rentaloTodoFrontend'
  .controller 'HomeController', ($scope, $auth, $rootScope, $document, $state, $mdSidenav, Restangular) ->
    'ngInject'
    $scope.url = "http://localhost:3000"
    $scope.logout = ->
      if $mdSidenav('left').isOpen()
        $mdSidenav('left').close()
      $auth.signOut().then ->
        $state.go 'home.landing'
        false
      false

    if $rootScope.user
      Restangular.one("me").get().then (profile) ->
        $scope.me = profile
        $rootScope.$broadcast('meGetted')

    $document.ready ->
      if $scope.user.signedIn && !$scope.user.email?
        $state.go "session.user_email"

    $scope.openUserPanel = ->
      $mdSidenav('left').toggle()
      false
    false

    $scope.openSideNavPanel = ->
      $mdSidenav('right').toggle()
      false
    false

    $scope.closeSideNavPanel = ->
      $mdSidenav('right').toggle()
      false
    false

    $scope.goHome = ->
      if $scope.user.signedIn
        $state.go "home.me.dashboard"
      else
        $state.go "home.landing"
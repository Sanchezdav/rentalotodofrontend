angular.module 'rentaloTodoFrontend'
  .config ($logProvider, toastrConfig) ->
    'ngInject'
    # Enable log
    $logProvider.debugEnabled true
    # Set options third-party lib
    toastrConfig.allowHtml = true
    toastrConfig.timeOut = 3000
    toastrConfig.positionClass = 'toast-top-right'
    toastrConfig.preventDuplicates = true
    toastrConfig.progressBar = true
  .config (RestangularProvider, apiUrl) ->
    RestangularProvider.setBaseUrl(apiUrl)
  .config ($mdThemingProvider) ->
    $mdThemingProvider.theme('default')
      .primaryPalette('orange')
      .accentPalette('blue')
      .warnPalette('red')
  .config ($authProvider, apiUrl)->
    $authProvider.configure
      apiUrl: apiUrl
      storage: 'localStorage'
      omniauthWindowType: 'sameWindow'
      tokenValidationPath: '/auth/validate_token',
      authProviderPaths:
        twitter:   '/auth/twitter'
        facebook: '/auth/facebook'
        google:   '/auth/google_oauth2'
  .config (laddaProvider) ->
    laddaProvider.setOption
      style: 'expand-right',
      spinnerSize: 35,
      spinnerColor: '#ffffff'
  .config (cfpLoadingBarProvider)->
    cfpLoadingBarProvider.includeSpinner = false
